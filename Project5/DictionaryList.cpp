#pragma once
#include "DictionaryList.h"
#include "algorithm"
#include <cstring>


DictionaryList::DictionaryList(const DictionaryList& src) 
{
	if (src.head_ == nullptr)
	{
		head_ = nullptr;
		return;
	}

	head_ = new Node({ src.head_->key_ });
	Node* srcPtr = src.head_->next_;
	Node* destPtr = head_;
	while (srcPtr != nullptr)
	{
		destPtr->next_ = new Node({ srcPtr->key_, nullptr });
		destPtr = destPtr->next_;
		srcPtr = srcPtr->next_;
	}
}

DictionaryList::DictionaryList(DictionaryList&& src) noexcept :
	head_(src.head_)
{
	src.head_ = nullptr;
}


DictionaryList& DictionaryList ::operator=(DictionaryList&& src) noexcept
{
	if (this != &src)
	{
		swap(src);
	}
	return *this;
}

void DictionaryList::swap(DictionaryList& other) noexcept
{
	std::swap(head_, other.head_);
}
DictionaryList::~DictionaryList() {
	Node* current = this->head();
	while (current != 0) {
		Node* next = current->next_;
		delete current;
		current = next;
	}
	head_ = nullptr;
}


DictionaryList::Node* DictionaryList::insert(DictionaryList::Node* x) {
	Node* ref = this->head_;
	Node* newNode = x;
	if (ref == nullptr) {
		this->head_ = newNode;
	}
	else {
		Node* current = ref;
		try {
			if (this->head_->key_ == newNode->key_) {
				throw "Key with this name already exists";
				return nullptr;
			}
			if (std::strcmp(this->head_->key_.c_str(), newNode->key_.c_str()) == 1) {
				newNode->next_ = ref;
				this->head_ = newNode;
			}
			else {
				while ((current->next_ != nullptr) && (std::strcmp(current->next_->key_.c_str(), newNode->key_.c_str())) == -1) {
					current = current->next_;
				}
				if ((current->next_ != nullptr) && (current->next_->key_ == newNode->key_)) {
					throw "Key with this name already exists";
					return nullptr;
				}
				newNode->next_ = current->next_;
				current->next_ = newNode;
			}
		}
		catch (const char* msg) {
			std::cerr << msg << "\n";
		}

	}
	return newNode;
}


void DictionaryList::del(Node* x) {
	Node* node = this->head_;
	if (node->key_ == x->key_) {
		Node* next = node->next_;
		delete node;
		this->head_ = next;
		return;
	}
	while ((node->next_ != nullptr) && (node->next_->key_ != x->key_)) {
		node = node->next_;
	}
	if ((node->next_ != nullptr) && (node->next_->key_ == x->key_)) {
		Node* next = node->next_->next_;
		delete node->next_;
		node->next_ = next;
	}
}


DictionaryList::Node* DictionaryList::search(Node* item) {
	Node* node = this->head_;
	if (node->key_ == item->key_) {
		return node;
	}
	while ((node->next_ != nullptr) && (node->next_->key_ != item->key_)) {
		node = node->next_;
	}

	if ((node->next_ != nullptr) && (node->next_->key_ == item->key_)) {
		return node->next_;
	}
	return nullptr;
}


bool DictionaryList::insert(std::string value) {
	Node* newNode = new Node({ value });	
	bool isSuccessful = this->insert(newNode);
	return isSuccessful;
}

void DictionaryList::del(std::string item) {
	Node* newNode = new Node({ item });
	this->del(newNode);
}

bool DictionaryList::search(std::string item) {
	Node* newNode = new Node({ item });
	Node* res = this->search(newNode);
	try {
		if (res != nullptr) {
			return true;
		}
		throw "No item with this key";
	}
	catch (const char* msg) {
		std::cerr << msg << "\n";
	}
	return false;

}

void DictionaryList::merge(DictionaryList& other) {
	while ((other.head_ != nullptr)) {
		Node* temp = other.head_->next_;
		this->insert(other.head_);
		other.head_ = temp;
	}
}


void DictionaryList::del(DictionaryList& other) {
	Node* current = other.head_;
	this->del(current->key_);
	while ((current->next_ != nullptr)) {
		this->del(current->next_->key_);
		current = current->next_;
	}
}


DictionaryList getIntersection(DictionaryList& one, DictionaryList& other) {
	DictionaryList res = DictionaryList();
	DictionaryList::Node* current = one.head_;
	if (other.search(current->key_)) {
		res.insert(current->key_);
	}
	while (current->next_ != nullptr) {
		if (other.search(current->next_->key_)) {
			res.insert(current->next_->key_);
		}
		current = current->next_;
	}
	return res;
}

void DictionaryList::outAll() {
	Node* iter = this->head_;
	if (iter != nullptr) {
		while (iter->next_ != nullptr) {
			std::cout << iter->key_ << " ";
			iter = iter->next_;
		}
		std::cout << iter->key_ << " ";
	}
	else {
		std::cout << "Empty Dictionary" << "\n";
	}
}