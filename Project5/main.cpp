#pragma once
#include "DictionaryList.h"

void delInsertSearchTest();
void mergeDeleteIntersection();


int main() {
	delInsertSearchTest();
	mergeDeleteIntersection();
	return 0;
};

void delInsertSearchTest() {
	std::cout << "Inserted 4 elements in empty Dict: ";
	DictionaryList i = DictionaryList();
	i.insert("0");
	i.insert("1");
	i.insert("50");
	i.insert("3");
	i.outAll();
	i.del("3");
	std::cout << "\nDeleted 3 from Dict: ";
	i.outAll();
	std::cout << "\nTrying to delete non-existing key: ";
	i.del("1241421124");
	std::cout << "Searching for 50 in Dict: " << ((i.search("50")) ? "50 found " : "50 not found ") << "\n";
	std::cout << "Searching for 1234 in Dict (No such key here): " << ((i.search("1234")) ? "1234 found " : "1234 not found ") << "\n";
}

void mergeDeleteIntersection() {
	std::cout << "Inserted 4 elements in empty Dict: ";
	DictionaryList i = DictionaryList();
	i.insert("0");
	i.insert("1");
	i.insert("50");
	i.insert("3");
	i.outAll();
	std::cout << "\nInserted 4 elements in empty Dict2: ";
	DictionaryList j = DictionaryList();
	j.insert("0");
	j.insert("999");
	j.insert("121");
	j.insert("3");
	j.outAll();
	std::cout << "\nMerging two Dicts: \n";
	i.merge(j);
	std::cout << "\nResult: ";
	i.outAll();
	j.outAll();
	std::cout << "\nNew dict: ";
	j.insert("0");
	j.insert("3");
	j.insert("10000");
	j.outAll();
	std::cout << "\nGet intersection: \n";
	DictionaryList a = getIntersection(i, j);
	std::cout << "New dict made with inersection method: ";
	a.outAll();
	std::cout << "\nDeleting intersection from dict: ";
	j.del(a);
	j.outAll();

}