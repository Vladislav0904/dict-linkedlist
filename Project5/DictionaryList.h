#pragma once
#include <iostream>
class DictionaryList
{
private:
	struct Node {
		std::string key_;
		Node* next_{ nullptr };
	};
	Node* head_;
	Node* head() const { return head_; }
	Node* insert(Node* x);
	void del(Node* x);
	Node* search(Node* item);

public:

	DictionaryList(const DictionaryList& src);
	DictionaryList(DictionaryList&& src) noexcept;
	DictionaryList& operator=(DictionaryList&& src) noexcept;
	~DictionaryList();
	DictionaryList() : head_(nullptr) {};


	bool insert(std::string value);
	void del(std::string item);
	bool search(std::string item);

	void merge(DictionaryList& other);
	void del(DictionaryList& other);
	void swap(DictionaryList& other) noexcept;
	friend DictionaryList getIntersection(DictionaryList& one, DictionaryList& other);
	int headItem() const { this->head(); };
	void outAll();


};

